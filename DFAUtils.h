#ifndef __DFAUTILS_H__
#define __DFAUTILS_H__

#include <stdio.h>
#include "cJSON.h"
#include "NFAUtils.h"
#include "DigitMapState.h"

#define IN 1
#define OUT 0

//struct1: DFA node (dfa node is a collection of nfa nodes)
struct DFANODE
{

    int nodeCode;

    int position;

    struct NFACOLLECTIONSINDFANODE * NFACollection;

    struct DFANODE * nextStates[18];

    int timer;

    //this pointer pointe to the next effective node
    //that is to say next line`s head node in dfa table
    struct DFANODE * nextEffectiveNode;

};

//struct2: DFA table (dfaTable is one row of dfatable)
struct DFATABLE
{
    int initFlag;//0 is uninited;1 was finished

    struct DFATABLE * nextLine;

    struct DFANODE * nodeArray[18];
};

//struct3: NFA link chain 
struct NFACOLLECTIONSINDFANODE
{
    struct NFANODE * nfaNode;
    struct NFACOLLECTIONSINDFANODE * next;
};

//struct4: DFA collection
//this struct can record multiple rulers
struct DFACOLLECTION
{
    struct DFANODE * ruler;
    struct DFACOLLECTION * nextRuler;
    cJSON * rulerConfigurationJson;
    //int index;
};

typedef struct RESULT
{
    cJSON * json;
    int result;
}RESULT;



//---------------------------------------------------

//func1: create a DFANODE
struct DFANODE * CreateDFANode();

//func : add a nfa node to dfaNode`s nfa collection
void PushNFANodeToDFANode(struct DFANODE * dfaNode,struct NFANODE * nfaNode);

//func :check nfa container (return 1: repeat ;return 0:un-repeat)
int CheckRepeatOfNFANode(struct NFACOLLECTIONSINDFANODE * NFACollection,struct NFANODE * targetNFA);

//func : get original node
struct DFANODE * GetOriginalDFANode(struct NFANODESTACK * startPointerArray);

//func : recursion find specific node(DFS)
void GetNextNFANodes(struct DFANODE *currentDFA ,struct NFANODE * currentNFA,int conditionCode);

//func : get next state DFA node with condition
struct DFANODE * GetDFANode(struct DFANODE *currentDFA,int conditionCode);

//func : create a DFA table
struct DFATABLE * CreateDFATable();

//func : insert a new line to DFA table;
void InsertLineToDFATable(struct DFATABLE * dfaTable,struct DFANODE * headDFANode);

//func : complete a line
void DFATableLineComplete(struct DFATABLE * dfaTableLine,struct DFATABLE * dfaTable);

//func : check the node duplication
struct DFANODE * CheckNodeDuplication(struct DFATABLE * dfaTable,struct DFANODE * dfaNode);

//func : build a DFA table
struct DFATABLE * GetDFATable(struct NFANODESTACK * nfaMap);

//func : free a DFA table
void ReleaseDFATable(struct DFATABLE * dfaTable);

//func : build DFA map;
struct DFANODE * GetDFAMap(struct DFATABLE * dfaTable);

//func : return match result for one ruler
int MatchARuler(struct DFANODE * dfaMap,char * input);

//func : return match result for all fulers
RESULT Match(char * input,int IN_OR_OUT);

//func : free all nfa nodes 
void ReleaseAllNFANodes(struct DFANODE * originalDFANode);

//func : initilize the rulers entity
void InitilizeRulers();

//func : append a new ruler
void AppendNewRuler(struct DFANODE * ruler,cJSON * json,int IN_OR_OUT);

//func : delete a ruler,note that ruler`s index start from 1;
void DeleteARuler(int index,int IN_OR_OUT);


/*
func : generate a new ruler base on input  
success ,return 1
fail , return -1
*/
int GenerateRuler(cJSON * json,int IN_OT_OUT);

#endif