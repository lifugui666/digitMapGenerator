#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <mcheck.h>
#include <string.h>

#include "NFAUtils.h"
#include "DFAUtils.h"
#include "cJSON.h"

void main()
{
    setenv("MALLOC_TRACE","MemCheck",1);
    mtrace();

    char input1[] = "{\
                    \"description\":\"for memorization\", \
					\"enabled\": false,                    \
					\"map\":\"182xxxxxxxx | 135xxxxxxxx | 192xxxxxxxx\",               \
					\"match_pattern\":\"1\",       \
					\"route\":\"1\",                  \
					\"action\": \"1\"                 \
				}";

    {
        cJSON* cjson1 = cJSON_Parse(input1); 
        GenerateRuler(cjson1,OUT);
    }

    char input2[] = "{  \"description\":\"for memorization\",\
					\"enabled\": false,                    \
					\"map\":\" 110 | 120 | 119\",                   \
					\"match_pattern\":\"2\",       \
					\"route\":\"2\",                  \
					\"action\":\"2\"                 \
				}";

    {
        cJSON* cjson2 = cJSON_Parse(input2);
        GenerateRuler(cjson2,OUT);
    }

    char input3[] = "{  \"description\":\"for memorization\",\
					\"enabled\": false,                    \
					\"map\":\" 10086x.s\",                   \
					\"match_pattern\":\"3\",       \
					\"route\":\"3\",                  \
					\"action\":\"3\"                 \
				}";

    {
        cJSON* cjson3 = cJSON_Parse(input3);
        GenerateRuler(cjson3,OUT);
    }

    char userInput[20];
    int inOrOutFlag = 0;//in = 1 ;out = 0;
    
    for(int i=0;i<10;i++)
    {
        memset(userInput,0,sizeof(userInput));
        fgets(userInput,20,stdin);
        
        RESULT result = Match(userInput,inOrOutFlag);
        printf("结果：%d\n",result.result);
        if(result.result != 504)
        {
            printf("匹配模式：%s\n",cJSON_GetObjectItem(result.json,"match_pattern")->valuestring);
            printf("语音链路：%s\n",cJSON_GetObjectItem(result.json,"route")->valuestring);
            printf("动作：%s\n",cJSON_GetObjectItem(result.json,"action")->valuestring);
        }
    }

    DeleteARuler(1,OUT);
    DeleteARuler(1,OUT);
    DeleteARuler(1,OUT);

}