#ifndef __NFAUTILS_H__
#define __NFAUTILS_H__

#include "DigitMapState.h"
#include <stdio.h>

//struct1: NFA node
struct NFANODE
{
    //contain 8 elements
    struct NFANODE * next1;
    struct NFANODE * next2;

    int edge1;
    int edge2;

    //不可能存在两个非epsilon的边，因此至多只需要一个输入匹配池记录匹配情况
    //digitMap本身允许的数据并不像正则表达式一样多...因此只需要使用一个long类型就可以按照位存储的方式进行存储
    //考虑到int类型在16位机上的长度问题，这里就使用了一个long类型；
    long int matchArray;
    
    //表示节点所处的位置(值可能是：起始位置，结束位置，其他位置)
    int position;
    
    //记录当前这个NFA节点哪一个DFA节点
    int DFASetIdentifier;

    //该节点的编号
    int nodeCode;
    //timer flag
    int timerFlag;
};

//struct2: contain a start node and a end node
struct STARTANDENDNODE
{
    struct NFANODE * startNode;
    struct NFANODE * endNode;
};
//struct3: NFA stack
struct NFANODESTACK
{
    struct NFANODESTACK * nextStack;
    struct STARTANDENDNODE * content;
    int linkeType;
};
//struct4: NFA Map`s start and end node

//util1：create a node
struct NFANODE * CreateANode();

//util2：create a start node and a end node
struct STARTANDENDNODE * CreateStartAndEnd();

//util3：digitMap grammer check
int CheckGrammer(char * originalString);

//util4：build nfa stack
struct NFANODESTACK * AnalysisToNFA(char * originalString);

//util5:link
    //5.1 epsilon
void EpsilonLink(struct NFANODE * parNode,struct NFANODE * subNode);
    //5.2 char set
void CharSetMatchLink(struct NFANODE * parNode,struct NFANODE * subNode);
    //5.3 single charactor
void SingleCharMatchLink(struct NFANODE * parNode,struct NFANODE * subNode);
    //5.4 multiple matcg(point)
void RepeatMatchLink(struct NFANODE * parNode,struct NFANODE * subNode,struct NFANODE * containParNod,struct NFANODE * containSubNode);

//util6: create a empty stack
struct NFANODESTACK * CreateAnEmptyStack();

//util7: push
void PushIntoNFAStack(struct NFANODESTACK * stack, struct STARTANDENDNODE * node,int linkType);

//util8:pop
struct NFANODESTACK * PopNFAStack(struct NFANODESTACK * stack);

//util9:free this stack
void DeleteNFASTACK(struct NFANODESTACK * stack);

//util10: transform NFA stack to NFA Map
struct NFANODESTACK * GetNFAMap(struct NFANODESTACK * stack);

//func11: release nfa chain
void ReleaseNFAChain(struct NFANODE * startNode);

#endif