#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include "NFAUtils.h"
#include "DFAUtils.h"
#include "DigitMapState.h"

/*static variable*/
static int nodeIdentifier=2;
static int MATCHPATTERN = LONGESTMATCH;

static struct DFACOLLECTION * incomingRulersRecorder = NULL;
static struct DFACOLLECTION * outgoingRulersRecorder = NULL;


/*
author:lfg
date:20-11-23
create a DFA node and return the address
*/
struct DFANODE * CreateDFANode()
{
    struct DFANODE * dfaNode = (struct DFANODE *)malloc(sizeof(struct DFANODE));
    dfaNode->nodeCode = -1;
    dfaNode->position = SEMIMATCH;
    dfaNode->timer = -1;
    struct NFACOLLECTIONSINDFANODE * temp = (struct NFACOLLECTIONSINDFANODE *)malloc(sizeof(struct NFACOLLECTIONSINDFANODE));
    temp->next=NULL;
    temp->nfaNode=NULL;
    dfaNode->NFACollection = temp;
    dfaNode->nextEffectiveNode = NULL;

    for(int i=0;i<18;i++)
        dfaNode->nextStates[i] = NULL;

    return dfaNode;
}

/*
author:lfg
date:20-11-24
add a nfa node to dfa node`s collection
*/
void PushNFANodeToDFANode(struct DFANODE * dfaNode,struct NFANODE * nfaNode)
{

    if(dfaNode==NULL)
        printf("invalid DFA node!\n");
    if(nfaNode==NULL)
        printf("invalid NFA node!\n");
    struct NFACOLLECTIONSINDFANODE * temp = (struct NFACOLLECTIONSINDFANODE *)malloc(sizeof(struct NFACOLLECTIONSINDFANODE));

    temp->nfaNode = nfaNode;
    temp->next = dfaNode->NFACollection->next;
    dfaNode->NFACollection->next = temp;
}

/*
author:lfg
date:20-11-24
1 means nfa collection has included this target nfa node
0 means nfa collection has not included this target nfa node
*/
int CheckRepeatOfNFANode(struct NFACOLLECTIONSINDFANODE * NFACollection,struct NFANODE * targetNFA)
{
    if(NFACollection->next == NULL)
    {
        return 0;
    }

    int flag = 0;
    struct NFACOLLECTIONSINDFANODE * temp = NFACollection->next;
    while(1)
    {
        
        if(temp==NULL)
            break;
        if( temp->nfaNode->nodeCode == targetNFA->nodeCode )
        {
            flag=1;
            return flag;
        }
        temp = temp->next;

        
    }
    return flag;
}

/*
author:lfg
date:20-11-24
get original dfa node
*/
struct DFANODE * GetOriginalDFANode(struct NFANODESTACK *startPointerArray)
{
    printf("GetOriginalDFANode  running ... \n");
    nodeIdentifier=2;//fisrt node code is 2(convenient to ^ opreate)
    struct DFANODE * resultDFANode = CreateDFANode();
    printf("dfa node create success\n");
    resultDFANode->position = STARTMATCH;
    //resultDFANode->nodeCode = nodeIdentifier;
    
    /*struct NFACOLLECTIONSINDFANODE * tempNFACOLLECTIONSINDFANODE = (struct NFACOLLECTIONSINDFANODE *)malloc(sizeof(struct NFACOLLECTIONSINDFANODE));
    tempNFACOLLECTIONSINDFANODE->next = NULL;
    resultDFANode->NFACollection = tempNFACOLLECTIONSINDFANODE;*/
    //nodeIdentifier++;

    struct NFANODESTACK * temp = NULL;

    while(1)
    {
        temp = PopNFAStack(startPointerArray);

        if(temp == NULL)
            break;
        
        if(temp->linkeType == TTIMERLINK || temp->linkeType == STIMERLINK || temp->linkeType == LTIMERLINK)
            resultDFANode->timer = temp->linkeType;

        PushNFANodeToDFANode(resultDFANode,temp->content->startNode);
        free(temp->content);
        free(temp);
    }
    free(startPointerArray);

    return resultDFANode;
}

/*
author:lfg
date:20-11-24
recursion,find next nfa node(DFS)
*/
void GetNextNFANodes(struct DFANODE *currentDFA,struct NFANODE * currentNFA,int conditionCode)
{
    if(currentNFA->edge1 != NIL)
    {
        if(currentNFA->edge1 == EPSILON_MATCH_DEGE)
        {
            GetNextNFANodes(currentDFA,currentNFA->next1,conditionCode);
        }
        else
        {
            if(currentNFA->edge1 == CHAR_SET_MATCH_EDGE)//char collection link
            {
                if( (currentNFA->matchArray & conditionCode) != 0)
                {
                    if(CheckRepeatOfNFANode(currentDFA->NFACollection,currentNFA->next1)==0)
                    {
                        PushNFANodeToDFANode(currentDFA,currentNFA->next1);
                        //if(currentNFA->next1->edge1 == NIL && currentNFA->next1->edge2 == NIL)

                        if(currentNFA->next1->timerFlag == TTIMERLINK 
                        || currentNFA->next1->timerFlag == STIMERLINK
                        || currentNFA->next1->timerFlag == LTIMERLINK
                        && currentDFA->position != FULLMATCH)
                        {
                            currentDFA->timer = currentNFA->next1->timerFlag;
                            currentDFA->position = TIMERMATCH;
                        }
                        else if(currentNFA->next1->position == ENDPOSITION)
                            currentDFA->position = FULLMATCH;
                    }
                }
                //else do nothing
            }
            else//single char matching condition
            {
                if(currentNFA->matchArray == conditionCode)
                {
                    if(CheckRepeatOfNFANode(currentDFA->NFACollection,currentNFA->next1)==0)
                    {
                        //this NFA node never appeared in this DFA node 
                        PushNFANodeToDFANode(currentDFA,currentNFA->next1);
                        //if(currentNFA->next1->edge1 == NIL && currentNFA->next1->edge2 == NIL)

                        if(currentNFA->next1->timerFlag == TTIMERLINK 
                        || currentNFA->next1->timerFlag == STIMERLINK
                        || currentNFA->next1->timerFlag == LTIMERLINK
                        && currentDFA->position != FULLMATCH)
                        {
                            currentDFA->timer = currentNFA->next1->timerFlag;
                            currentDFA->position = TIMERMATCH;
                        }
                        else if(currentNFA->next1->position == ENDPOSITION)
                            currentDFA->position = FULLMATCH;

                        
                    }
                }
            }
        }
    }
    if(currentNFA->edge2 != NIL)
    {
        if(currentNFA->edge2 == EPSILON_MATCH_DEGE)
        {
            GetNextNFANodes(currentDFA,currentNFA->next2,conditionCode);
        }
        else
        {
            if(currentNFA->edge2 == conditionCode)//char collection 
            {
                if(currentNFA->matchArray & conditionCode != 0)
                {
                    if(CheckRepeatOfNFANode(currentDFA->NFACollection,currentNFA->next2)==0)
                    {
                        PushNFANodeToDFANode(currentDFA,currentNFA->next2);
                        //if(currentNFA->next2->edge1 == NIL && currentNFA->next2->edge2 == NIL)
                        
                        if(currentNFA->next2->timerFlag == TTIMERLINK 
                        || currentNFA->next2->timerFlag == STIMERLINK 
                        || currentNFA->next2->timerFlag == LTIMERLINK
                        && currentDFA->position != FULLMATCH)
                        {
                            currentDFA->timer = currentNFA->next2->timerFlag;
                            currentDFA->position = TIMERMATCH;
                        }
                        else if(currentNFA->next2->position == ENDPOSITION)
                            currentDFA->position = FULLMATCH;
                    }
                }
            }
            else//single char matching condition
            {
                if(currentNFA->matchArray == conditionCode)
                {
                    if(CheckRepeatOfNFANode(currentDFA->NFACollection,currentNFA->next2)==0)
                    {
                        PushNFANodeToDFANode(currentDFA,currentNFA->next2);
                        //if(currentNFA->next2->edge1 == NIL && currentNFA->next2->edge2 == NIL)

                        if(currentNFA->next2->timerFlag == TTIMERLINK 
                        || currentNFA->next2->timerFlag == STIMERLINK
                        || currentNFA->next2->timerFlag == LTIMERLINK
                        && currentDFA->position != FULLMATCH)
                        {
                            currentDFA->timer = currentNFA->next2->timerFlag;
                            currentDFA->position = TIMERMATCH;
                        }
                        else if(currentNFA->next2->position == ENDPOSITION)
                            currentDFA->position = FULLMATCH;
                    }
                }
            }
        }
    }
    return;
}

/*
author:lfg
date:20-11-24
create next collection with given conditions
this condition does not contain char collection link
cndition code: 0 1<0;1 1<1;...e.t.c
*/
struct DFANODE * GetDFANode(struct DFANODE * HeadDFA,int ConditionCode)
{
    struct DFANODE * resultDFANode = CreateDFANode();

    if(HeadDFA == NULL)
    {
        printf("head node is not exist!\n");
        free(resultDFANode->NFACollection);
        free(resultDFANode);
        return NULL;
    }

    struct NFACOLLECTIONSINDFANODE * tempFlag = HeadDFA->NFACollection->next;

    if(tempFlag == NULL)
    {
        printf("there is a empty DFA node\n");
        free(resultDFANode->NFACollection);
        free(resultDFANode);
        return NULL;
    }

    while(1)
    {
        if(tempFlag == NULL)
            break;
        //else
        //resultDFANode is a empty node, this node will be full with NFANode message;
        //tempFlang is a nfa collection,this collection contain all nfa node of head dfa node;
        GetNextNFANodes(resultDFANode,tempFlag->nfaNode,ConditionCode);

        tempFlag = tempFlag->next;
    }

    if(resultDFANode->NFACollection->next==NULL)
    {
        free(resultDFANode->NFACollection);
        free(resultDFANode);
        return NULL;
    }
    else
        return resultDFANode;
}


/*
author:lfg
date:20-11-24
create a DFA table , first and last rows are empty
*/
struct DFATABLE * CreateDFATable()
{
    struct DFATABLE * dfaTable = (struct DFATABLE *)malloc(sizeof(struct DFATABLE));
    dfaTable->nextLine=NULL;
    dfaTable->initFlag=0;
    for(int i=0;i<18;i++)
        dfaTable->nodeArray[i]=NULL;
    return dfaTable;
}

/*
author:lfg
date:20-11-24
insert a new line to DFA table;
*/
void InsertLineToDFATable(struct DFATABLE * dfaTable,struct DFANODE * headDFANode)
{
    struct DFATABLE * newDFALTableLine = (struct DFATABLE *)malloc(sizeof(struct DFATABLE));
    newDFALTableLine->initFlag=0;
    newDFALTableLine->nextLine=NULL;

    for(int i=0;i<18;i++)
        newDFALTableLine->nodeArray[i] = NULL;
        
    newDFALTableLine->nodeArray[0] = headDFANode;
    headDFANode->nodeCode = nodeIdentifier;
    nodeIdentifier++;
    struct DFATABLE * tempDFALTableLine = dfaTable;
    while(1)
    {
        //find last line
        if(tempDFALTableLine->nextLine == NULL)
        {
            tempDFALTableLine->nextLine = newDFALTableLine;
            //record next effective dfa node,build a linked list...
            if(tempDFALTableLine->nodeArray[0] != NULL)
                tempDFALTableLine->nodeArray[0]->nextEffectiveNode = newDFALTableLine->nodeArray[0];
            break;
        }
        else
        {
            tempDFALTableLine = tempDFALTableLine->nextLine;
        }
    }
}

/*
author:lfg 
date:20-11-24
complete one line of DFA table;
*/
void DFATableLineComplete(struct DFATABLE * dfaTableLine,struct DFATABLE * dfaTable)
{
    struct DFANODE * headNode = dfaTableLine->nodeArray[0];
    struct DFANODE * tempNode = NULL;
    for(int i=1;i<18;i++)
    {
        if(i == 11)
            continue;
        
        tempNode = GetDFANode(headNode,1<<(i-1));
        if(tempNode == NULL)
        {
            dfaTableLine->nodeArray[i] = NULL;
        }
        else
        {
            tempNode = CheckNodeDuplication(dfaTable,tempNode);
            dfaTableLine->nodeArray[i] = tempNode;
            //tempNode->nodeCode = nodeIdentifier;
            //nodeIdentifier++;
        }

    }
    dfaTableLine->initFlag = 1;
}

/*
author:lfg
date:20-11-25
check the duplication of dfa node in dfa table
if duplication is esixted,reutrn address 
if it dose not exist ,insert a new line;return address
*/
struct DFANODE * CheckNodeDuplication(struct DFATABLE * dfaTable,struct DFANODE * dfaNode)
{
    if(dfaTable == NULL)
    {
        printf("invalid DFA table\n");
        return NULL;
    }
    if(dfaNode == NULL)
    {
        printf("there is a empty DFA node\n");
        return NULL;
    }

    struct NFACOLLECTIONSINDFANODE * tempNFANodeOfHeadNode = NULL; 
    struct NFACOLLECTIONSINDFANODE * tempNFANodeOfCurrentDFANode = NULL;
    struct DFANODE * tempDFAHeadNode = NULL;
    struct DFATABLE * tempLine = NULL;//dfaTable->nextLine;
    
    int containedInThisHeadNode = 0;
    int repeatInThisTable = 0;
    int currentDFAIsDiffWithHeadNode = 0;

    int lengthOfDFANode = 0;
    int lengthOfHeadNode = 0;
    
    //get length of DFANode
    tempNFANodeOfCurrentDFANode = dfaNode->NFACollection->next;
    while(1)
    {
        if(tempNFANodeOfCurrentDFANode==NULL)
        {
            tempNFANodeOfCurrentDFANode = NULL;
            break;
        }
        lengthOfDFANode++;
        tempNFANodeOfCurrentDFANode = tempNFANodeOfCurrentDFANode->next;
    }

    //go though all line
    tempLine = dfaTable->nextLine;
    while(1)
    {
        if(tempLine == NULL)
            break;
        //get and check length of head node 
        tempDFAHeadNode = tempLine->nodeArray[0];
        tempNFANodeOfHeadNode = tempDFAHeadNode->NFACollection->next;
        lengthOfHeadNode = 0;
        while(1)
        {
            if(tempNFANodeOfHeadNode == NULL)
            {
                break;
            }
            lengthOfHeadNode++;
            tempNFANodeOfHeadNode = tempNFANodeOfHeadNode->next;
        }
        if(lengthOfHeadNode != lengthOfDFANode)
        {
            tempLine = tempLine->nextLine;
            continue;
        }
                  
        tempNFANodeOfCurrentDFANode = dfaNode->NFACollection->next;
        currentDFAIsDiffWithHeadNode = 0;
        while(1)
        {
            if(tempNFANodeOfCurrentDFANode==NULL)
            {
                break;
            }
            //go though nfa nodes which are included by head node 
            tempNFANodeOfHeadNode = tempDFAHeadNode->NFACollection->next;
            containedInThisHeadNode =0;
            while(1)
            {
                if(tempNFANodeOfHeadNode == NULL)
                {
                    break;
                }
                //compare 
                if(tempNFANodeOfHeadNode->nfaNode->nodeCode == tempNFANodeOfCurrentDFANode->nfaNode->nodeCode)
                {
                    containedInThisHeadNode = 1;
                    break;
                }
                tempNFANodeOfHeadNode = tempNFANodeOfHeadNode->next;
            }
            if(containedInThisHeadNode == 0)
            {
                currentDFAIsDiffWithHeadNode =1;
            }
            tempNFANodeOfCurrentDFANode = tempNFANodeOfCurrentDFANode->next;
        }

        if(currentDFAIsDiffWithHeadNode == 0)
        {

            struct NFACOLLECTIONSINDFANODE * nfaNodeCollector = dfaNode->NFACollection;
            struct NFACOLLECTIONSINDFANODE * nfaNodeCollector2 = NULL;
            while(1)
            {
                if(nfaNodeCollector->next == NULL)
                    break;
                nfaNodeCollector2 = nfaNodeCollector->next;
                nfaNodeCollector->next = nfaNodeCollector2->next;
                free(nfaNodeCollector2);
            }
            free(nfaNodeCollector);
            free(dfaNode);

            return tempLine->nodeArray[0];
        }
        
        tempLine = tempLine->nextLine;
    }

    InsertLineToDFATable(dfaTable,dfaNode);
    return dfaNode;
}

/*
author:lfg
date20-11-23
establish a DFA Node table,this param is infact a array
*/
struct DFATABLE * GetDFATable(struct NFANODESTACK * nfaMap)
{
    struct DFATABLE * resultDFATable = CreateDFATable();
    struct DFANODE * originalDFANode = GetOriginalDFANode(nfaMap);
    if(originalDFANode != NULL)
    InsertLineToDFATable(resultDFATable,originalDFANode);
    struct DFATABLE * tempDFATableLine = resultDFATable->nextLine;
    if(tempDFATableLine->nodeArray[0]!=NULL)

    while(1)
    {
        printf("build line\n");
        DFATableLineComplete(tempDFATableLine,resultDFATable);
        if(tempDFATableLine->nextLine == NULL)
            break;
        else
            tempDFATableLine = tempDFATableLine->nextLine;
        
    }

    //free all nfa node 
    ReleaseAllNFANodes(originalDFANode);

    return resultDFATable;
}

/*
author:lfg
date:20-11-30
build DFA map by DFA table
*/
struct DFANODE * GetDFAMap(struct DFATABLE * dfaTable)
{
    if(dfaTable == NULL)
    {
        printf("dfa table is empty\n");
        return NULL;
    }
    struct DFATABLE * tempLine = dfaTable->nextLine;
    struct DFANODE * DFAMap = tempLine->nodeArray[0];
    struct DFANODE * tempHeadNode = NULL;

    while(1)
    {
        if(tempLine == NULL)
            break;
        tempHeadNode = tempLine->nodeArray[0];
        for(int i=0 ; i<17 ; i++)
        {
            if(tempLine->nodeArray[i+1] != NULL)
            {
                tempHeadNode->nextStates[i] = tempLine->nodeArray[i+1];
            }
        }
        
        tempLine = tempLine->nextLine;
    }

    ReleaseDFATable(dfaTable);
    return DFAMap;
}

/*
author: lfg
date:20-12-01
release specific dfa table 
note that this func`s work is just release the relationship of dfa table line;the dfa node 
which contained by dfa table still keep exist;
*/
void ReleaseDFATable(struct DFATABLE * dfaTable)
{
    struct DFATABLE * tempLine = dfaTable->nextLine;
    struct DFATABLE * tempLine2 = NULL;
    struct NFACOLLECTIONSINDFANODE * nfaNodeCollector = NULL;
    struct NFACOLLECTIONSINDFANODE * nfaNodeCollector2 = NULL;
    while(1)
    {
        if(tempLine == NULL)
            break;
        
        tempLine2 = tempLine;
        tempLine = tempLine->nextLine;

        nfaNodeCollector = tempLine2->nodeArray[0]->NFACollection;

        while(1)
        {
            if(nfaNodeCollector->next == NULL)
                break;
            nfaNodeCollector2 = nfaNodeCollector->next;
            nfaNodeCollector->next = nfaNodeCollector2->next;
            free(nfaNodeCollector2);
        }
        free(nfaNodeCollector);
        //free this line
        free(tempLine2);
    }
    free(dfaTable);
}

/*
author:lfg
date:20-11-30
return match result for one ruler

501 Full
502 Timer: 403 T | 404 S | 405 L
503 semi
504 miss

*/
int MatchARuler(struct DFANODE * dfaMap,char * input)
{
    int result = MISMATCH;
    struct DFANODE * currentNode = dfaMap;
    struct DFANODE * nextNode = NULL;
    int lengthOfInput = strlen(input);
    for(int i=0;i<lengthOfInput;i++)
    {
        if(input[i]=='\n')
            break;
        //preprocess
        switch(input[i])
        {
            case 'a':
                input[i] = 'A';
                break;
            case 'b':
                input[i] = 'B';
                break;
            case 'c':
                input[i] = 'C';
                break;
            case 'd':
                input[i] = 'D';
                break;
            case 'e':
                input[i] = 'E';
                break;
            case '*':
                input[i] = 'E';
                break;
            case 'f':
                input[i] = 'F';
                break;
            case '#':
                input[i] = 'F';
                break;
        }

        if('0' <= input[i] && input[i] <= '9')//'0' == 48
        {
            nextNode = currentNode->nextStates[input[i]-48];

            if(nextNode != NULL)
            {
                if(nextNode->position == FULLMATCH)
                {
                    result = FULLMATCH;
                }
                if(nextNode->position == TIMERMATCH)
                {
                    if(nextNode->timer == TTIMERLINK)
                        result = TTIMERLINK;
                    if(nextNode->timer == STIMERLINK)
                        result = STIMERLINK;
                    if(nextNode->timer == LTIMERLINK)
                        result = LTIMERLINK;
                }
                if(nextNode->position == SEMIMATCH)
                {
                    result = SEMIMATCH;
                }
                
                currentNode = nextNode;
            }
            else
            {
                result = MISMATCH;
                break;
            }
        }
        
        if('A' <= input[i] && input[i] <= 'F')// 'A' == 65,but i need 'A'-54 = 11;
        {
            nextNode = currentNode->nextStates[input[i]-54];

            if(nextNode != NULL)
            {
                if(nextNode->position == FULLMATCH)
                {
                    result = FULLMATCH;
                }
                if(nextNode->position == TIMERMATCH)
                {
                    if(nextNode->timer == TTIMERLINK)
                        result = TTIMERLINK;
                    if(nextNode->timer == STIMERLINK)
                        result = STIMERLINK;
                    if(nextNode->timer == LTIMERLINK)
                        result = LTIMERLINK;
                }
                if(nextNode->position == SEMIMATCH)
                {
                    result = SEMIMATCH;
                }

                currentNode = nextNode;
            }
            else
            {
                result = MISMATCH;
                break;
            }
        }

    }
    return result;
}

/*
author:lfg
date:20-12-03
return all rulers match result,return final result;
*/
RESULT Match(char * input,int IN_OR_OUT)
{
    RESULT r;
    r.result = MISMATCH;
    r.json = NULL;

    if(input == NULL)
        return r;

    struct DFACOLLECTION * tempRuler = NULL;
    if(IN_OR_OUT == IN && incomingRulersRecorder != NULL)
    {
        tempRuler = incomingRulersRecorder->nextRuler;
    }
    else if(IN_OR_OUT == OUT && outgoingRulersRecorder != NULL)
    {
        tempRuler = outgoingRulersRecorder->nextRuler;
    }
    else
    {
        return r;
    }
    
    int result = MISMATCH;
    while(1)
    {
        if(tempRuler == NULL)
            break;
        result = MatchARuler(tempRuler->ruler,input);
        if(result != MISMATCH)
            break;
        //else
        tempRuler = tempRuler->nextRuler;
    }
    //return result;
    if(result != MISMATCH)
        r.json = tempRuler->rulerConfigurationJson;
    else//result = mismatch
        r.json = NULL;
    
    r.result = result;
    
    return r;
}

/*
author: lfg
date : 20-12-01
release all nfa nodes(nfa node is unnecessary when )
*/
void ReleaseAllNFANodes(struct DFANODE * originalDFANode)
{
    struct NFACOLLECTIONSINDFANODE * nfaNodeCollector = originalDFANode->NFACollection->next;

    while(1)
    {
        if(nfaNodeCollector == NULL)
        {
            break;
        }
        
        ReleaseNFAChain(nfaNodeCollector->nfaNode);

        nfaNodeCollector = nfaNodeCollector->next;
    }

}


/*
author: lfg
date : 20-12-03
initilize the rulers entity
*/
void InitilizeRulers(struct DFACOLLECTION * rulersRecorder)
{
    rulersRecorder = (struct DFACOLLECTION *)malloc(sizeof(struct DFACOLLECTION));
    rulersRecorder->ruler = NULL;
    rulersRecorder->nextRuler = NULL;
    rulersRecorder->rulerConfigurationJson = NULL;
    //rulersRecorder->index = 0;
}

/*
author : lfg
date : 20-12-03
append a new ruler to rulers
*/
void AppendNewRuler(struct DFANODE * ruler,cJSON * json,int IN_OR_OUT)
{
    struct DFACOLLECTION * tempElement = NULL;

    //analysis json,apeend ruler to right collection 
    //struct DFACOLLECTION * rulersRecorder = NULL;
    if(IN_OR_OUT == IN)
    {
        if(incomingRulersRecorder == NULL)
        {
            //InitilizeRulers(incomingRulersRecorder);
            {
                incomingRulersRecorder = (struct DFACOLLECTION *)malloc(sizeof(struct DFACOLLECTION));
                incomingRulersRecorder->ruler = NULL;
                incomingRulersRecorder->nextRuler = NULL;
                incomingRulersRecorder->rulerConfigurationJson = NULL;
            }
            tempElement = (struct DFACOLLECTION *)malloc(sizeof(struct DFACOLLECTION));
            tempElement->ruler = ruler;
            tempElement->rulerConfigurationJson = json;
            tempElement->nextRuler = NULL;
            //tempElement->index = 1;
            incomingRulersRecorder->nextRuler = tempElement;
        }
        else
        {
            tempElement = incomingRulersRecorder;
            while(1)
            {
                if(tempElement->nextRuler == NULL)
                {
                    tempElement->nextRuler = (struct DFACOLLECTION *)malloc(sizeof(struct DFACOLLECTION));
                    tempElement->nextRuler->ruler = ruler;
                    tempElement->nextRuler->rulerConfigurationJson = json;
                    tempElement->nextRuler->nextRuler = NULL;
                    //tempElement->nextRuler->index = i;
                    break;
                }
                tempElement = tempElement->nextRuler;
            }
        }
    }
    else// outgoing
    {
        if(outgoingRulersRecorder == NULL)
        {
            //InitilizeRulers(outgoingRulersRecorder);
            {
                outgoingRulersRecorder = (struct DFACOLLECTION *)malloc(sizeof(struct DFACOLLECTION));
                outgoingRulersRecorder->ruler = NULL;
                outgoingRulersRecorder->nextRuler = NULL;
                outgoingRulersRecorder->rulerConfigurationJson = NULL;
            }
            tempElement = (struct DFACOLLECTION *)malloc(sizeof(struct DFACOLLECTION));
            tempElement->ruler = ruler;
            tempElement->rulerConfigurationJson = json;
            tempElement->nextRuler = NULL;
            outgoingRulersRecorder->nextRuler = tempElement;
        }
        else
        {
            tempElement = outgoingRulersRecorder;
            while(1)
            {
                if(tempElement->nextRuler == NULL)
                {
                    tempElement->nextRuler = (struct DFACOLLECTION *)malloc(sizeof(struct DFACOLLECTION));
                    tempElement->nextRuler->ruler = ruler;
                    tempElement->nextRuler->rulerConfigurationJson = json;
                    tempElement->nextRuler->nextRuler = NULL;
                    //tempElement->nextRuler->index = i;
                    break;
                }
                tempElement = tempElement->nextRuler;
            }
        }
    }
}

/*
author : lfg 
date : 20-12-03
delete a ruler,if rulers become empty collection after delete,release rulers entity;
delete by index, ruler`s index start from 1
*/
void DeleteARuler(int index,int IN_OR_OUT)
{
    struct DFACOLLECTION * rulersRecorder = NULL;
    if(IN_OR_OUT == IN)
        rulersRecorder = incomingRulersRecorder;
    else
        rulersRecorder = outgoingRulersRecorder;
    
    if(rulersRecorder == NULL || rulersRecorder->nextRuler == NULL)
    {
        printf("have no rulers\n");
        return;
    }
    if(index < 1)
    {
        printf("error index\n");
        return;
    }

    struct DFACOLLECTION * tempElement = rulersRecorder;

    //note that tempElement->nextRuler after the loop is the target;
    for(int i=1;i<index;i++)
    {
        if(tempElement->nextRuler == NULL)
        {
            printf("delete faile,ruler index is %d is not exis\n",index);
            return;
        }
        tempElement = tempElement->nextRuler;
    }

    if(tempElement->nextRuler == NULL)
    {
        printf("out of rulers range\n");
        return;
    }
    else//tempElement -> nextRuler != NULL
    {
        //pick target ruler and re-link splited portions
        struct DFACOLLECTION * tempElement2 = tempElement->nextRuler;
        tempElement->nextRuler = tempElement->nextRuler->nextRuler;
        //free target ruler( tempElements2 )
        struct DFANODE * tempDFANode = tempElement2->ruler;
        struct DFANODE * tempDFANode2 = NULL;
        while(1)
        {
            if(tempDFANode->nextEffectiveNode == NULL)
            {
                //release current node 
                free(tempDFANode);
                break;
            }
            tempDFANode2 = tempDFANode;
            tempDFANode = tempDFANode->nextEffectiveNode;
            free(tempDFANode2);
        }
        cJSON_Delete(tempElement2->rulerConfigurationJson);
        free(tempElement2);
    }

    //if rulersRecoder is a empty after delete,free memory
    if(rulersRecorder->nextRuler == NULL)
    {
        free(rulersRecorder);
    }
}

/*
author : lfg
date: 20-12-03
GENERATE A RULER BASED ON INPUT!!!!
succes,return 1
fail,return -1
*/
int GenerateRuler(cJSON * json,int IN_OR_OUT)
{

    char* input = cJSON_GetObjectItem(json,"map")->valuestring;//analysis this json get digitMap  

    if(input == NULL)//if the string contained in json is empty, there are no ruler to append;
        return -1;
    
    int flag = CheckGrammer(input);

    if(flag!=SUCCESS)
    {
        printf("It doesn't conform to the basic grammar\n");
        free(input);//release memory
        return -1;
    }

    struct NFANODESTACK * stack = NULL;
    
    stack = AnalysisToNFA(input);
    

    //build NFA 
    struct NFANODESTACK * NFAMap = GetNFAMap(stack);

    if(NFAMap == NULL)
        return -1;

    struct DFATABLE * dfaTable = GetDFATable(NFAMap);

    //build DFA
    struct DFANODE * DFAMap = GetDFAMap(dfaTable);

    //insert a ruler into ruler collection;
    AppendNewRuler(DFAMap,json,IN_OR_OUT);
    
    return 1;
}

