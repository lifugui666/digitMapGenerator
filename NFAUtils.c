#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include "NFAUtils.h"
#include "DigitMapState.h"

/*static variable*/
static int NFANodeCode=0;
static int lengthOfArray=0; //  length of input
static int sizeOfStartPointerArray=0;

/*
auther:lfg
date:20-11-20
check input string for DigitMap`s basic syntax
*/
int CheckGrammer(char *originalString)
{
    //check input char array length
    //int lengthOfArray=0;
    lengthOfArray = 0;
    lengthOfArray = strlen(originalString);

    if(lengthOfArray<=0)
    {
        printf("input is empty!!!\n");
        return INPUT_IS_EMPTY;
    }

    int analysisStateFlg = STARTPOSITION;//process pointer position flag
    char lastEffectiveValue=0;
    for(int i=0;i<lengthOfArray;i++)
    {
        switch(originalString[i])
        {
            case ' ':break;// ignore space
            case 0:break;// ignore NULL
            case 127:break;// ignore backspace
            case '\r':break;// ignore line feeds
            case '\n':break;//  ignore carriage returns
            case '0':lastEffectiveValue='0';break;
            case '1':lastEffectiveValue='1';break;
            case '2':lastEffectiveValue='2';break;
            case '3':lastEffectiveValue='3';break;
            case '4':lastEffectiveValue='4';break;
            case '5':lastEffectiveValue='5';break;
            case '6':lastEffectiveValue='6';break;
            case '7':lastEffectiveValue='7';break;
            case '8':lastEffectiveValue='8';break;
            case '9':lastEffectiveValue='9';break;
            case 'x':
            {
                lastEffectiveValue='X';
                originalString[i]='X';
                if(analysisStateFlg==DMAP_RANGE_START)
                {
                    printf("x/X can not be contained by []\n");
                    return RANGE_GRAMMER_ERROR;
                }
                break;
            }
            case 'X':
            {   
                lastEffectiveValue='X';

                break;
            }
            case 'a':lastEffectiveValue='A';originalString[i]='A';break;
            case 'A':lastEffectiveValue='A';break;
            case 'b':lastEffectiveValue='B';originalString[i]='B';break;
            case 'B':lastEffectiveValue='B';break;
            case 'c':lastEffectiveValue='C';originalString[i]='C';break;
            case 'C':lastEffectiveValue='C';break;
            case 'd':lastEffectiveValue='D';originalString[i]='D';break;
            case 'D':lastEffectiveValue='D';break;
            case '*':lastEffectiveValue='E';originalString[i]='E';break;
            case 'e':lastEffectiveValue='E';originalString[i]='E';break;
            case 'E':lastEffectiveValue='E';break;
            case '#':lastEffectiveValue='F';originalString[i]='F';break;
            case 'f':lastEffectiveValue='F';originalString[i]='F';break;
            case 'F':lastEffectiveValue='F';break;
            case '[':
            {
                if(analysisStateFlg == DMAP_RANGE_START)
                {
                    printf("there are 2 successive character:[\n");
                    return RANGE_GRAMMER_ERROR;
                }
                else
                    analysisStateFlg = DMAP_RANGE_START;
                
                lastEffectiveValue='[';
                break;
            }
            case '-':
            {
                if(analysisStateFlg != DMAP_RANGE_START)
                {
                    printf("- must be in []\n");
                    return RANGE_GRAMMER_ERROR;
                }
                if(lastEffectiveValue>'9' || lastEffectiveValue<'0')
                {
                    printf("character must be a number from 0 to 9 which behind -\n");
                    return RANGE_GRAMMER_ERROR;
                }
                if(i+1>=lengthOfArray||(originalString[i+1]>'9'||originalString[i+1]<'0'))
                {
                    printf("character must be a number form 0 ot 9 which after -\n");
                    return RANGE_GRAMMER_ERROR;
                }
                if(originalString[i-1]>=originalString[i+1])
                {
                    printf("character after - must be larger than character behind -\n");
                    return RANGE_GRAMMER_ERROR;
                }
                lastEffectiveValue='-';
                break;
            }
            case ']':
            {
                if(lastEffectiveValue == '[')
                {
                    printf("cannot be empty between [ and ]\n");
                    return RANGE_IS_EMPTY;
                }
                if(analysisStateFlg != DMAP_RANGE_START)
                {
                    printf("] must be after [\n");
                    return RANGE_GRAMMER_ERROR;
                }
                else
                {
                    analysisStateFlg = SUCCESS;
                }
                
                lastEffectiveValue=']';
                break;
            }
            case 'T':lastEffectiveValue='T';break;
            case 't':lastEffectiveValue='T';originalString[i]='T';break;
            case 'L':lastEffectiveValue='L';break;
            case 'l':lastEffectiveValue='L';originalString[i]='L';break;
            case 'S':lastEffectiveValue='S';break;
            case 's':lastEffectiveValue='S';originalString[i]='S';break;
            case '|':
            {
                if(lastEffectiveValue == 0)
                {
                    printf("| cannot be used as the begining in whole string\n");
                    return SPLIT_CANT_BE_FIRST;
                }
                if(lastEffectiveValue == '.')
                {
                    printf(". cannot be used as the ending\n");
                    return POINT_CANT_BE_ENDING;
                }
                
                if(analysisStateFlg == DMAP_RANGE_START)
                {
                    printf("every [ needs a matching ]\n");
                    return RANGE_GRAMMER_ERROR;
                }
                else
                {
                    analysisStateFlg = STARTPOSITION;
                }
                lastEffectiveValue='|';
                break;
            }
            case '.':
            {
                
                if(lastEffectiveValue=='|'||lastEffectiveValue==0)
                {
                    printf(". cant be used as the begining in any sub-string\n");
                    return POINT_CANT_BE_FIRST;
                }
                if(lastEffectiveValue=='.')
                {
                    printf("two consecutive . are not allowed");
                    return POINT_CANT_BE_CONSECUTIVE;
                }
                lastEffectiveValue='.';
                break;
            }
            default:
            {
                printf("%c(ASCII:%d) is not a effective charater of GiditMap!!!\n",originalString[i],originalString[i]);
                return DIGITMAP_CHARACTER_ERROR;
            }

        }
    }
    if(lastEffectiveValue=='|')
    {
        printf("| cant be used as the ending in whole string\n");
        return SPLIT_CANT_BE_LAST;
    }
    if(lastEffectiveValue=='.')
    {
        printf(". cant be used as the ending\n");
        return POINT_CANT_BE_ENDING;
    }
    analysisStateFlg = STARTPOSITION;
    return SUCCESS;
}

/*
author:lfg
date:20-11-19
create a NFA node
*/
struct NFANODE * CreateANode()
{
    struct NFANODE * nfaNode = (struct NFANODE *)malloc(sizeof(struct NFANODE));
   	
    nfaNode->next1 = NULL;
    nfaNode->next2 = NULL;

    nfaNode->edge1 = NIL;
    nfaNode->edge2 = NIL;

    nfaNode->matchArray=0;//init to 0(even in 16bit long int`s binary code also is 0000 0000 0000 0000 0000 0000 0000 0000)

    nfaNode->position = OTHERSPOSITION;

    nfaNode->DFASetIdentifier=-1;//init to -1, this property will be really init in dfa portion;

    nfaNode->nodeCode = NFANodeCode;
    NFANodeCode++;

    nfaNode->timerFlag=-1;//init to have not timer;

    return nfaNode;
}

/*
author:lfg
date:20-11-19
keep reference of start node and end node
*/
struct STARTANDENDNODE * CreateStartAndEnd()
{
    struct STARTANDENDNODE * startAndEndNode = (struct STARTANDENDNODE *)malloc(sizeof(struct STARTANDENDNODE));
    startAndEndNode->startNode = CreateANode();
    startAndEndNode->startNode->position = STARTPOSITION;
    startAndEndNode->endNode = CreateANode();
    startAndEndNode->endNode->position = ENDPOSITION;
    return startAndEndNode;
}

/*
author:lfg
date:20-11-20
link 2 nodes with epsilon edge
*/
void EpsilonLink(struct NFANODE *parNode,struct NFANODE *subNode)
{
    if(parNode->edge1==NIL && parNode->next1==NULL)
    {
        parNode->edge1 = EPSILON_MATCH_DEGE;
        parNode->next1 = subNode;
    }
    else
    {
        parNode->edge2 = EPSILON_MATCH_DEGE;
        parNode->next2 = subNode;
    }   
}

/*
author:lfg
date:20-11-20
link 2 nodes with char set edge
*/
void CharSetMatchLink(struct NFANODE * parNode,struct NFANODE * subNode)
{
    if(parNode->edge1==NIL && parNode->next1==NULL)
    {
        parNode->edge1 = CHAR_SET_MATCH_EDGE;
        parNode->next1 = subNode;
    }
    else
    {
        parNode->edge2 = CHAR_SET_MATCH_EDGE;
        parNode->next2 = subNode;
    }
    
}

/*
author:lfg
date:20-11-20
link 2 nodes with single char edge
*/
void SingleCharMatchLink(struct NFANODE * parNode,struct NFANODE * subNode)
{
    if(parNode->edge1==NIL && parNode->next1==NULL)
    {
        parNode->edge1 = SINGLE_CHAR_MATCH_EDGE;
        parNode->next1 = subNode;
    }
    else
    {
        parNode->edge2 = SINGLE_CHAR_MATCH_EDGE;
        parNode->next2 = subNode;
    }
    
}

/*
author:lfg
date:20-11-20
link 2 nodes with repeatable edge
params: parNode emptyNode 
        subNode enptyNode
        containParNode last state`s first Node
        containSubNode last state`s second Node
*/
void RepeatMatchLink(struct NFANODE * parNode,
                    struct NFANODE * subNode,
                    struct NFANODE * containParNode,
                    struct NFANODE * containSubNode)
{
    EpsilonLink(parNode,containParNode);
    EpsilonLink(parNode,subNode);
    EpsilonLink(containSubNode,subNode);
    EpsilonLink(containSubNode,containParNode);
}

/*
author:lfg
date:20-11-20
analysis string to NFA stack
*/
struct NFANODESTACK * AnalysisToNFA(char * originalString)
{
    //int analysisToken=-1;
    //init nfa node code to 2,convenient to compare the result of xor
    NFANodeCode=2;
    //create start and end Nodes pointer，this pointer will be inited to null
    //struct STARTANDENDNODE * NFAMap = NULL;
    struct NFANODESTACK * nfaStack = CreateAnEmptyStack();
    int linkType = NORMALLINK;

    //analysis
    for(int analysisIndex=0 ; analysisIndex<lengthOfArray ; analysisIndex++)
    {

        //ignore:space,NULL,backspace,line feeds,carriage returns
        if( originalString[analysisIndex]==' '||originalString[analysisIndex]==0||originalString[analysisIndex]==127 ||originalString[analysisIndex]==10||originalString[analysisIndex]==13)
            continue;

        struct STARTANDENDNODE * currentNFARelationship = CreateStartAndEnd();
        
        //check current char`s token
        switch(originalString[analysisIndex])
        {
            case '0':  
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_0;
                break;
            }
            case '1':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_1;
                break;
            }
            case '2':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_2;
                break;
            }
            case '3':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_3;
                break;
            }
            case '4':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_4;
                break;
            }
            case '5':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_5;
                break;
            }
            case '6':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_6;
                break;
            }
            case '7':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_7;
                break;
            }
            case '8':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_8;
                break;
            }
            case '9':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_9;
                break;
            }
            case 'X':
            {
                CharSetMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                for(int i=0;i<10;i++)//x flag also tune to 1
                    currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | (1<<i);
                break;
            }
            case 'A':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_A;
                break;
            }
            case 'B':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_B;
                break;
            }
            case 'C':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_C;
                break;
            }
            case 'D':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_D;
                break;
            }
            case 'E':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_E;
                break;
            }
            case 'F':
            {
                SingleCharMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_F;
                break;
            }
            case '[':
            {
                CharSetMatchLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                while(1)
                {
                    analysisIndex++;
                    if(originalString[analysisIndex]==']')
                        break;
                    
                    if(originalString[analysisIndex]=='-')
                    {
                        for(int i=originalString[analysisIndex-1];i<=originalString[analysisIndex+1];i++)
                        {
                            currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | 1<<(i-48) ;
                        }
                    }
                    else
                    {
                        switch(originalString[analysisIndex])
                        {
                            case '0':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_0;
                                break;
                            }
                            case '1':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_1;
                                break;
                            }
                            case '2':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_2;
                                break;
                            }
                            case '3':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_3;
                                break;
                            }
                            case '4':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_4;
                                break;
                            }
                            case '5':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_5;
                                break;
                            }
                            case '6':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_6;
                                break;
                            }
                            case '7':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_7;
                                break;
                            }
                            case '8':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_8;
                                break;
                            }
                            case '9':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_9;
                                break;
                            }
                            case 'A':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_A;
                                break;
                            }
                            case 'B':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_B;
                                break;
                            }
                            case 'C':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_C;
                                break;
                            }
                            case 'D':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_D;
                                break;
                            }
                            case 'E':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_E;
                                break;
                            }
                            case 'F':
                            {
                                currentNFARelationship->startNode->matchArray=currentNFARelationship->startNode->matchArray | DMAP_F;
                                break;
                            }
                        }
                    }
                }
                break;
            }
            case '.':
            {
                EpsilonLink(currentNFARelationship->startNode,currentNFARelationship->endNode);
                linkType=POINTLINK;
                break;
            }
            case '|':
            {
                linkType=SPLITLINK;
                break;
            }
            case 'T':
            {
                linkType = TTIMERLINK;
                break;
            }
            case 'S':
            {
                linkType = STIMERLINK;
                break;
            }
            case 'L':
            {
                linkType = LTIMERLINK;
                break;
            }
        }

        PushIntoNFAStack(nfaStack,currentNFARelationship,linkType);
        linkType = NORMALLINK;
    }

    //when the loop ends nfaStack point to stack top
    printf("NFA was builded\n");
    return nfaStack;

}

/*
author:lfg
date:20-11-21
create an empty stack
*/
struct NFANODESTACK * CreateAnEmptyStack()
{
    struct NFANODESTACK * stack = (struct NFANODESTACK *)malloc(sizeof(struct NFANODESTACK));

    stack->nextStack = NULL;
    stack->linkeType = 0;

    return stack;
}

/*
author:lfg
date:20-11-21
NFA stack push
*/
void PushIntoNFAStack(struct NFANODESTACK * stack, struct STARTANDENDNODE * node,int linkType)
{
    struct NFANODESTACK * headStack = (struct NFANODESTACK *)malloc(sizeof(struct NFANODESTACK));

    headStack->content = node;
    headStack->linkeType = linkType;

    headStack->nextStack = stack->nextStack;
    stack->nextStack = headStack;
}

/*
author:lfg
date:20-11-21
NFA stack pop & return the element which was poped;
!!! note that this element will not be released!!!
*/
struct NFANODESTACK * PopNFAStack(struct NFANODESTACK * stack)
{

    if(stack == NULL)
    {
        printf("this stack is non-existent\n");
        return NULL;
    }
    if(stack->nextStack==NULL)
    {
        printf("this stack has no contents\n");
        return NULL;
    }

    struct NFANODESTACK * tempNode = stack->nextStack ;

    stack->nextStack = tempNode->nextStack;

    return  tempNode;
}

/*
author:lfg
date:20-11-21
free specific stack 
*/
void DeleteNFASTACK( struct NFANODESTACK * stack)
{
    if(stack==NULL)
    {
        printf("this stack is non-existent\n");
        return;
    }
    if(stack->nextStack == NULL)
    {
        free(stack);
        printf("NFA stack has been released\n");
        return;
    }
    struct NFANODESTACK * pop = NULL;
    while(1)
    {
        pop = PopNFAStack(stack);
        if(pop != NULL)
        {
            printf("release: %ld \n",pop->content->startNode->matchArray);
            free(pop);
        }
        if(pop == NULL)
        {
            free(stack);
            printf("NFA stack has been released\n");
            return;
        }       
    }

}

/*
author:lifugui
date:20-11-21
transform NFA stack to NFA Map
this func value returned is a array;this array is not use malloc;
*/
struct NFANODESTACK * GetNFAMap(struct NFANODESTACK * stack)
{

    if(stack==NULL || stack->nextStack==NULL)
    {
        printf("error:A empty stack can not be used to build a NFA map\n");
        return NULL;
    }

    int timerFlag = -1;

    struct NFANODESTACK * temp = stack->nextStack;

    struct NFANODESTACK * startPointerArray = CreateAnEmptyStack();

    while(1)
    {
        temp = PopNFAStack(stack);
        //target timer messaage
        if(temp->linkeType == TTIMERLINK)
        {
            //free this flag node 
            free(temp->content->startNode);
            free(temp->content->endNode);
            free(temp->content);
            free(temp);
            timerFlag = TTIMERLINK;
            continue;
        }
        if(temp->linkeType == STIMERLINK)
        {
            free(temp->content->startNode);
            free(temp->content->endNode);
            free(temp->content);
            free(temp);
            timerFlag = STIMERLINK;
            continue;
        }
        if(temp->linkeType == LTIMERLINK)
        {
            free(temp->content->startNode);
            free(temp->content->endNode);
            free(temp->content);
            free(temp);
            timerFlag = LTIMERLINK;
            continue;
        }
        //mark current node if timer is effective
        if(timerFlag != -1)
        {
            temp->content->endNode->timerFlag = timerFlag;
            temp->content->startNode->timerFlag = timerFlag;
        }

        //build map
        if(stack->nextStack == NULL)
        {
            //startPointerArray[tempIndex] = temp->content.startNode;
            PushIntoNFAStack(startPointerArray,temp->content,0);
            //free(temp->content);
            free(temp);
            //clear timer flag
            timerFlag = -1;

            break;
        }
        if(stack->nextStack->linkeType == SPLITLINK)
        {
            //startPointerArray[tempIndex] = temp -> content.startNode;
            PushIntoNFAStack(startPointerArray,temp->content,0);
            //free(temp->content);
            free(temp);
            temp = PopNFAStack(stack);
            free(temp->content->startNode);
            free(temp->content->endNode);
            free(temp->content);
            free(temp);
            //clear timer flag 
            timerFlag = -1;

            continue;
        }
        
        //temp have next node and next node is`t '|'

        if(temp->linkeType == NORMALLINK)
        {
            stack->nextStack->content->endNode->position = OTHERSPOSITION;
            temp->content->startNode->position = OTHERSPOSITION;                

            EpsilonLink(stack->nextStack->content->endNode,temp->content->startNode);
            free(temp->content);
            free(temp);
            timerFlag = -1;
        }

        if(temp->linkeType == POINTLINK)
        {
            stack->nextStack->content->startNode->timerFlag = timerFlag;
            stack->nextStack->content->endNode->timerFlag = timerFlag;

            stack->nextStack->content->startNode->position = OTHERSPOSITION;
            //stack->nextStack->content->endNode->position = OTHERSPOSITION;
            temp->content->endNode->position = OTHERSPOSITION;

            EpsilonLink(temp->content->startNode,stack->nextStack->content->startNode);
            EpsilonLink(stack->nextStack->content->endNode,stack->nextStack->content->startNode);
            EpsilonLink(stack->nextStack->content->endNode,temp->content->endNode);
            
            if(stack->nextStack->nextStack == NULL)//1.2
            {   
                PushIntoNFAStack(startPointerArray,temp->content,0);
                free(stack->nextStack->content);
                free(stack->nextStack);
                free(temp);
                timerFlag = -1;
                break;
            }
            else if(stack->nextStack->nextStack->linkeType == SPLITLINK)// xxx | 1.2
            {
                PushIntoNFAStack(startPointerArray,temp->content,0);
                free(temp);
                //get node(1) and release it expect 2 nodes;
                temp = PopNFAStack(stack);
                free(temp->content);
                free(temp);
                //get split node and release it totally,include 2 nodes are contained in the split node
                temp = PopNFAStack(stack);
                free(temp->content->startNode);
                free(temp->content->endNode);
                free(temp->content);
                free(temp);
                //reset timer flag
                timerFlag = -1;
            }
            else// 12.3 or 123| 45.6
            {
                /*free(PopNFAStack(stack));
                EpsilonLink(stack->nextStack->content->endNode,temp->content->startNode);
                free(temp->content);
                free(temp);*/
                EpsilonLink(stack->nextStack->nextStack->content->endNode,temp->content->startNode);
                stack->nextStack = temp->nextStack->nextStack;
                free(temp->nextStack->content);
                free(temp->nextStack);
                free(temp->content);
                free(temp);

                timerFlag = -1;
            }
        }

    }
    
    free(stack);

    return startPointerArray;
}

/*
author:lfg
date:20-12-01
recursion, release a chain
*/
void ReleaseNFAChain(struct NFANODE * startNode)
{
    if(startNode->next1 == NULL && startNode->next2 == NULL)
    {
        free(startNode);
        return;
    }

    if(startNode->next2 != NULL)
    {
        ReleaseNFAChain(startNode->next2);
        free(startNode);
        return; 
    }
    if(startNode->next1 != NULL)
    {
        ReleaseNFAChain(startNode->next1);
        free(startNode);
        return;
    }
}