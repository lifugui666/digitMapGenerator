#ifndef __DIGITMAPSTATE_H__
#define __DIGITMAPSTATE_H__

enum DIGITMAPSTATE
{
    DMAP_0=1,               //0x0
    DMAP_1=1<<1,            //0x1
    DMAP_2=1<<2,            //0x4
    DMAP_3=1<<3,            //0x8
    DMAP_4=1<<4,            //0x10
    DMAP_5=1<<5,            //0x20
    DMAP_6=1<<6,            //0x40
    DMAP_7=1<<7,            //0x
    DMAP_8=1<<8,
    DMAP_9=1<<9,
    DMAP_X=1<<10,           //infact x is a range(from 0 to 9)

    DMAP_A=1<<11,
    DMAP_B=1<<12,           //12
    DMAP_C=1<<13,           //13
    DMAP_D=1<<14,           //14
    DMAP_E=1<<15,			//* or E	15
    DMAP_F=1<<16,			//# or F	16
    
    DMAP_RANGE_START=1<<17,	//[ 17
    DMAP_DASH=1<<18,	    //-	18
    DMAP_RANGE_END=1<<19,	//]	19
    DMAP_TIMER_T=1<<20,		//T 20
    DMAP_TIMER_L=1<<21,		//L 21
    DMAP_TIMER_S=1<<22,		//S 22
    DMAP_SPLIT=1<<23,       //| 23 split 
    DMAP_POINT=1<<24        //. 24 repeat
};


enum MATCHSTATE
{
    NIL=100,	                    //NULL
    CHAR_SET_MATCH_EDGE=101,	    //char collection
    SINGLE_CHAR_MATCH_EDGE=102,	    //single char
    EPSILON_MATCH_DEGE=103 	        //epsilon
};

enum DIGITMAPERRORSTATE
{

    SUCCESS=200,             
    INPUT_IS_EMPTY=201,
    RANGE_GRAMMER_ERROR=202,
    POINT_CANT_BE_FIRST=203,
    DIGITMAP_CHARACTER_ERROR=204,
    POINT_CANT_BE_CONSECUTIVE=205,
    SPLIT_CANT_BE_FIRST=206,
    RANGE_IS_EMPTY=207,
    SPLIT_CANT_BE_LAST=208,
    POINT_CANT_BE_ENDING=209
};

enum SEPCIALPOSITION
{
    STARTPOSITION=300,     //char currently being process is first char
    OTHERSPOSITION=301,    //char currently being process is`t first
    ENDPOSITION=302
};

enum STACKLINKTYPE
{
    NORMALLINK=400,        //normal linke
    POINTLINK=401,         //repeatable linke
    SPLITLINK=402,         //split 
    TTIMERLINK=403,         //T timer
    STIMERLINK=404,         //S timer
    LTIMERLINK=405          //L timer
};

enum DFANODEMATCHSTATE
{
    STARTMATCH=500,         //original 
    FULLMATCH=501,          //full 
    TIMERMATCH=502,         //timeout 
    SEMIMATCH=503,          //half 
    MISMATCH=504            //miss
};

enum MATCHLENGTH
{
    LONGESTMATCH=600,
    SHORTESTMATCH=601
};

#endif

            