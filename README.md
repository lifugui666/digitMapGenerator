## Basic Description
1. Generate a DFA based on digitMap;
2. Check user input with DFA;
3. Multiple digitMap rulers are allowed;
4. Early input rules take precedence;
## Result Of Check
1. prefect match (501)
2. T timer timeout match (403)
3. S timer timeout match (404)
4. L timer timeout match (405)
6. Semi match    (503)
7. Mismatch      (504)