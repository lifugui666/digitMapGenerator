test : test.o NFAUtils.o DFAUtils.o 
	gcc  -g -o test test.o NFAUtils.o DFAUtils.o cJSON.o

cJSON.o : cJSON.c
	gccc -c cJSON.c

DFAUtils.o : DFAUtils.c
	gcc  -c DFAUtils.c

NFAUtils.o : NFAUtils.c
	gcc  -c NFAUtils.c

test.o : test.c
	gcc  -c test.c
