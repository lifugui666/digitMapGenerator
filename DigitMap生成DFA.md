# DigitMap匹配工具



## 使用说明

### 规则的插入和删除

1. 使用函数GenerateRuler(cJSON * json,int IN_OR_OUT)添加一条规则，json指针需要指向digitmap.profile.outgoing或者digitmap.profile.incoming中的元素；如果需要插入的是outgoing的规则，IN_OR_OUT的值设置为OUT，反之设置为IN；（IN=1；OUT=0）；添加成功返回1，添加失败返回-1；
2. json指针将会被incomingRulersRecorder或者outgoingRulersRecorder保存起来，当该条规则被删除的时候会将这个json对象会被一并释放；
3. **支持多个DigitMap规则同时存在，他们会分别被保存在DFAUtils.c的incomingRulersRecorder和outgoingRulersRecorder中，添加规则的时候使用了尾插，因此后插入的规则优先级会低于先前插入的规则；**
4. 删除一个规则的时候同样需要指定被删除的规则是属于incoming还是outgoing；

###  DigitMap输入处理

1. 在对字符串进行正式处理前，会进行一次基本语法检查；只接受字符0-9，a-f，A-F，|，[，]，-，#，*，T，S，L，t，s，l，X，x；同时可以忽略DigitMap中的' ',0,127,'\r','\n'；如果出现其余字符会报错；（NFAUtils.c的方法：CheckGrammer（））；
2. 如果通过了基础语法检查，那么DigitMap string中的所有小写字母会被替换成大写字母，*会被地换成E，#会被替换成F；
3. DigitMap string中的元素入栈，这个过程中，计时器和分割符等特殊字符也会被当作一个元素入栈；每一个元素包含两个NFA节点；

### DigitMap string -> DFA

1. 出栈，生成NFA，这个过程中，分隔符和计时器一类的特殊元素会被转化成其他有效元素中的属性；同时这些特殊元素占用的内存将被释放；出栈过程中会把相邻的元素链接起来，组成一个NFA；
2. 根据NFA生成一个原始DFA节点；此时DFA节点中包含着NFA节点；
3. 根据原始DFA节点中的NFA节点，遍历NFA，类似于深度优先搜索；根据已有的DFA节点寻找新的节点，如果找到新节点，DFA表扩建一行并将新节点作为新一行的头节点；完成每一行，直到最后找不出新节点；（**DFA节点中保存了下一行的头节点的地址，释放内存的时候直接按照这个指针一路释放就完事了**）
4. **范围匹配的情况下，直接把代表范围匹配的节点添加到每个在这个范围内的条件中，例如NFA节点a是[2-4]，会在2，3，4对应的DFA节点中都添加a；**
5. **在DFA表中，只有每一行的头节点（第一列的节点）是真正占用了内存的，头节点之外的数据都是指针**
6. 完成DFA表之后，根据DFA表将存在的DFA节点链接起来，生成DFA，同时释放掉DFA表占用的内存和DFA中留存的NFA有关的内存；届时，DFA以一系列DFA节点的形式存在，通过起始节点访问该DFA；

### 呼入 | 呼出匹配

1. 调用方法RESULT Match(char * input,int IN_OR_OUT)进行匹配，第一个参数是用户输入，第二个参数决定此次匹配是incoming还是outgoing；

2. 针对呼入和呼出的字符串也会做一个预处理，会将小写字符统一转换为大写，*替换为E，#替换为F；

3. 返回值结构体RESULT，包含一个指向json的指针和一个int类型的result；通过json能获取到匹配到的规则的配置，通过result能获取到匹配结果；**如果结果是MISMATCH（504），那么RESULT.json==NULL**；如果对应的rulerRecorder是NULL，也会返RESULT.json==NULL，RESULT.result=504;

   > 返回值含义：
   >
   > | 值   | 枚举       |      含义       |
   > | ---- | ---------- | :-------------: |
   > | 501  | FULLMATCH  |    完全匹配     |
   > | 403  | TTIMERLINK | T计时器超时匹配 |
   > | 404  | STIMERLINK | S计时器超时匹配 |
   > | 405  | LTIMERLINK | L计时器超时匹配 |
   > | 503  | SEMIMATCH  |     半匹配      |
   > | 504  | MISMATCH   |     不匹配      |

4. 用户输入读取使用了strlen（如果输入中存在\n一类的字符会被计入并且处理）；

5. 针对一个字符串进行处理（字符指针），如果DTMF_BUF有更新的话请以整个DTMF_BUF中的内容作为参数重新进行匹配；

6. 如果用户输入是NULL，直接返回不匹配；

7. 会对字符串中的内容挨个进行处理，如果有一个元素使状态达到了不匹配，则直接返回；

8. 类似于12.s这种DigitMap，输入1会被视作完全匹配，输入12222会被视作计时器匹配；

9. 如果存在多条规则，则优先使用更早插入的规则，如果某个规则匹配失败，顺位向下使用下一个规则进行匹配，如果都匹配失败，那就真的失败了....

### 删除已存在的规则

1. 调用方法DeleteARuler(int index,int IN_OR_OUT)；index用于表示想要删除的是第几条规则；

2. **规则的编号从1开始**，删除的本质是从链表中剔除，同时会释放掉这个元素中包含的json指针指向的内存；

3. **例如：如果incoming中存在两条规则，想要将两条全部删除**

   > ```c
   > DeleteARuler(1,IN);//执行完之后剩下的那条规则index由2->1;
   > DeleteARuler(1,IN);
   > ```
   >
   > 当index=1的元素被删除后，原先index=2的元素的index会变成1；

4. **删除方法并没有将改动同步到文件系统；这个删除方法仅仅是释放了指定的DFA占用的内存**

## 依赖说明

### 文件说明

1. NFAUtils；负责将字符串转化为NFA
2. DFAUtils；负责将NFA转化为DFA，并且将DFA保存起来，同时负责完成匹配工作；

### 依赖

需要cJSON.h

## 内存泄漏分析

```bash
lfg@PS2020MEJAZGGP:/mnt/c/Users/Admin/Documents/DIGITMAP$ mtrace  test MemCheck
- 0x00007fffcf283260 Free 4212 was never alloc'd 0x7fc29df9a58d
- 0x00007fffcf283320 Free 4213 was never alloc'd 0x7fc29df1dbae
- 0x00007fffcf283340 Free 4214 was never alloc'd 0x7fc29df9a578
No memory leaks.
```

在删除掉全部规则的情况下，没有发生内存泄漏事件；



